#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main() {
	srand(time(NULL));

	int* arr = (int*)malloc(4 * 4 * sizeof(int));

	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			*(arr + i * 4 + j) = -100 + rand() % 200;
			printf("%3d | ", *(arr + i * 4 + j));
		}
		printf("\n");
	}

	int* arrMax = (int*) malloc(4 * sizeof(int));
	for (int i = 0; i < 4; i++) arrMax[i] = *(arr + i);

	printf("\n");
	for (int j = 0; j < 4; j++) {
		for (int i = 0; i < 4; i++) {
			if (*(arr + i * 4 + j) > arrMax[j]) arrMax[j] = *(arr + i * 4 + j);
		}
		printf("%3d -- %d\n", arrMax[j], arrMax + j);
	}


	return 0;
}