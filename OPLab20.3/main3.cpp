#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main() {
	srand(time(NULL));

	int a, b;
	do {printf("a = "); scanf("%d", &a); } while (a <= 0);
	do {printf("b = "); scanf("%d", &b); } while (b <= 0);
	int* x = (int*)malloc(a * sizeof(int));
	int* y = (int*)malloc(b * sizeof(int));

	printf("x:\n");
	for (int i = 0; i < a; i++){x[i] = -99 + rand() % 200; printf("%3d | ", x[i]);}
	printf("\nt:\n");
	for (int i = 0; i < b; i++){y[i] = -99 + rand() % 200; printf("%3d | ", y[i]);}

	int* xADDy = (int*)malloc((a + b) * sizeof(int));
	int k = 0;

	while(k < a) {
		xADDy[k] = x[k];
		k++;
	}
	for (int i = 0; i < b; i++) {
		xADDy[k] = y[i];
		k++;
	}
	printf("\nx + y:\n");
	for (int i = 0; i < k; i++) printf("%3d | ", xADDy[i]);

	int* xANDy = (int*)malloc((a>b?b:a) * sizeof(int));
	int t = 0;

	for (int i = 0; i < a; i++) {
		for (int j = 0; j < b; j++) {
			if (x[i] == y[j]) { xANDy[t] = x[i]; t++; break; }
		}
	}

	realloc(xANDy, t * sizeof(int));
	printf("\nx and y:\n");
	for (int i = 0; i < t; i++) printf("%3d | ", xANDy[i]);

	return 0;
}