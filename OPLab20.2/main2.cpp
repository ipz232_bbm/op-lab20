#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int main() {
	srand(time(NULL));

	int n, m;
	do {
		printf("Array[n][m] (n > 0, m > 0):\nn = ");
		scanf("%d", &n);
		printf("m = ");
		scanf("%d", &m);
	} while (n <= 0 || m <= 0);

	int** arr = (int**)malloc(n * sizeof(int*));

	for (int i = 0; i < n; i++) {
		arr[i] = (int*)malloc(m * sizeof(int));
		for (int j = 0; j < m; j++) {
			arr[i][j] = -99 + rand() % 200;
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}

	int nc;
	do {
		printf("Add nc column. nc = ");
		scanf("%d", &nc);
	} while (nc < 0 || nc > m);

	m++;

	for (int i = 0; i < n; i++) realloc(arr[i], m * sizeof(int));

	for (int i = 0; i < n; i++) {
		for (int j = m; j >= nc; j--) {
			arr[i][j] = arr[i][j - 1];
		}
	}

	for (int i = 0; i < n; i++) {
		arr[i][nc] = -99 + rand() % 200;;
	}

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}

	int deleteRow = -1;

	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (arr[i][j] == 0) {
				deleteRow = i;
				break;
			}
		}
		if (deleteRow != -1) break;
	}
	printf("%d", deleteRow);

	n--;
	for (int i = deleteRow; i < n; i++) {
		for (int j = 0; j < m; j++) {
			arr[i][j] = arr[i + 1][j];
		}
	}
	realloc(arr, n * sizeof(int*));
	printf("\n");
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			printf("%3d | ", arr[i][j]);
		}
		printf("\n");
	}

	return 0;
}